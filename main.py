from functions import m_json
from functions import m_pck

##Pfade zu den Setup-Dateien
path_1 = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
path_2 = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"

##Sensoren übreprüfen
m_pck.check_sensors()

##Metadaten aus json file auslesen
#metadata_1 = m_json.get_metadata_from_setup(path_1)
metadata_2 = m_json.get_metadata_from_setup(path_2)


##Anfügen der Seriennummer der Sensoren an die Metadaten
#m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', metadata_1)
m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', metadata_2)


##Erstellen der json-Datei
#m_json.archiv_json('/home/pi/calorimetry_home/datasheets', path_1, '/home/pi/calorimetry_home/archiv/Newton_Experiment')
m_json.archiv_json('/home/pi/calorimetry_home/datasheets', path_2, '/home/pi/calorimetry_home/archiv/Heat_capacity_Experiment')


##Sammlen der Messdaten
#data_1 = m_pck.get_meas_data_calorimetry(metadata_1)
data_2 = m_pck.get_meas_data_calorimetry(metadata_2)


##Abspeichern der Messdaten in der json-Datei
#m_pck.logging_calorimetry(data_1, metadata_1, '/home/pi/calorimetry_home/archiv/Newton_Experiment', '/home/pi/calorimetry_home/datasheets')
m_pck.logging_calorimetry(data_2, metadata_2, '/home/pi/calorimetry_home/archiv/Heat_capacity_Experiment', '/home/pi/calorimetry_home/datasheets')

print(":)")
