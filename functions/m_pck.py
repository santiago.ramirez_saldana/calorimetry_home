import os
import sys
import time
import json

from w1thermsensor import Sensor, W1ThermSensor
import h5py
import numpy as np


# This if statement enables you to write and run programs that test functions directly at the end of this file.
if __name__ == "__main__":
    import pathlib

    file_path = os.path.abspath(__file__)
    file_path = pathlib.Path(file_path)
    root = file_path.parent.parent
    sys.path.append(str(root))

from functions import m_json


def check_sensors() -> None:
    """Retrieves and prints the serial number and current temperature of all DS18B20 Temperature Sensors.

    This function utilizes the `w1thermsensor` library to interface with the DS18B20 temperature sensors.
    For each available sensor, it prints its serial number (or ID) and the current temperature reading.

    Examples:
    Assuming two DS18B20 sensors are connected:
    >>> check_sensors()
    Sensor 000005888445 has temperature 25.12
    Sensor 000005888446 has temperature 24.89

    """
    # TODO: Print serials and temperature values of all connected sensors.
    sensors = W1ThermSensor.get_available_sensors()
    
    #überprüfen ob Sesoren angeschlossen sind
    if not sensors:
        print("Keine Sensoren gefunden")
        return
    
    else: 
        print("Es gibt:",len(sensors))

    # Informatioon von den Sensoren entnehmen
    for i in sensors:
        print ("Sensor Nr:",i)
        serial_number = i.id
        temperature = i.get_temperature()
        print(" Der Sensor",serial_number, "hat die Tempertatur",temperature,"°C")
    # DONE #


def get_meas_data_calorimetry(metadata: dict) -> dict:
    """Collects and returns temperature measurement data from DS18B20 sensors based on the provided metadata.

    This function initializes sensor objects based on the metadata, prompts the user to start the
    measurement, and then continually reads temperature values until interrupted (e.g., via Ctrl-C).
    It logs the temperatures and the corresponding timestamps. Refer to README.md section
    "Runtime measurement data" for a detailed description of the output data structure.

    Args:
        metadata (dict): Contains details like sensor uuid, serials, and names.
                         Refer to README.md section "Runtime metadata" for a detailed description
                         of the input data structure.

    Returns:
        dict:
        {'group_info': {'UUID': ['1ee80b26-48bd-602d-9d9d-40f01e977671'], 'name': ['group_info']},
        'instrument': {'UUID': ['1ee80b26-aad9-653d-af60-03de2f3b9040'], 'name': ['raspberry_pi']},
        'sensor': {'UUID': ['1ee7d8b5-89ef-698d-b53c-47a54e651099', '1ee7d8cb-01f3-6155-bf64-7d793842269d'],
        'name': ['temperature_cup_1', 'temperature_cup_2'], 'serial': ['3ce104574fd4', '3ce1e3805090']},
        'probe': {'UUID': ['1ee80b27-5e16-6232-becb-381aa0d45873'], 'name': ['probe_description']},
        'choose your own type, if you miss one': {'UUID': ['add_more_here_if_you_need'], 'name': ['']}}

    Example:
        Input metadata:
        {
            "sensor": {
                "values": ["sensor_1", "sensor_2"],
                "serials": ["000005888445", "000005888446"],
                "names": ["FrontSensor", "BackSensor"]
            }
        }

        Output (example data after interruption):
        {
            "sensor_1": [[25.12, 25.15], [0, 2]],
            "sensor_2": [[24.89, 24.92], [0, 2]]
        }

    """
    # Initialize an empty dictionary for storing temperature measurements.
    # The structure is uuid: [[temperatures], [timestamps]].
    data = {i: [[], []] for i in metadata["sensor"]["UUID"]}
    start = time.time()
    sensor_list = [
        W1ThermSensor(Sensor.DS18B20, id) for id in metadata["sensor"]["serial"]
    ]
    input("Press any key to start measurement... <Ctrl+C> to stop measurement")
    try:
        while True:
            for i, sensor in enumerate(sensor_list):
                # TODO: Get experimental data.
                temperature = sensor.get_temperature()
                data[metadata["sensor"]["UUID"][i]][0].append(temperature)
                data[metadata["sensor"]["UUID"][i]][1].append(time.time() - start)
                print(f"Sensor: {metadata['sensor']['UUID'][i]}, Temperature: {temperature:.2f}°C")




                # DONE #
            # Print an empty line for better readability in the console.
            print("")
    # Catch the KeyboardInterrupt (e.g., from Ctrl-C) to stop the measurement loop.
    except KeyboardInterrupt:
        # Print the collected data in a formatted JSON structure.
        print(json.dumps(data, indent=4))
    # Always execute the following block.
    finally:
        # Ensure that the lengths of temperature and timestamp lists are the same for each sensor.
        for i in data:
            # If the temperature list is longer, truncate it to match the length of the timestamp list.
            if len(data[i][0]) > len(data[i][1]):
                data[i][0] = data[i][0][0 : len(data[i][1])]
            # If the timestamp list is longer, truncate it to match the length of the temperature list.
            elif len(data[i][0]) < len(data[i][1]):
                data[i][1] = data[i][1][0 : len(data[i][0])]
    print(data)
    return data
    


def logging_calorimetry(
    data: dict,
    metadata: dict,
    data_folder: str,
    json_folder: str,
) -> None:
    """Logs the calorimetry measurement data into an H5 file.

    This function creates a folder (if not already present) and an H5 file with a
    specific structure. The data from the provided dictionaries are written to the
    H5 file, along with several attributes.

    Args:
        data (dict): get_meas_data_calorimetry({'group_info': {'UUID': ['1ee80b26-48bd-602d-9d9d-40f01e977671'], 'name': ['group_info']},
        'instrument': {'UUID': ['1ee80b26-aad9-653d-af60-03de2f3b9040'], 'name': ['raspberry_pi']},
        'sensor': {'UUID': ['1ee7d8b5-89ef-698d-b53c-47a54e651099', '1ee7d8cb-01f3-6155-bf64-7d793842269d'],
        'name': ['temperature_cup_1', 'temperature_cup_2'], 'serial': ['3ce104574fd4', '3ce1e3805090']},
        'probe': {'UUID': ['1ee80b27-5e16-6232-becb-381aa0d45873'], 'name': ['probe_description']},
        'choose your own type, if you miss one': {'UUID': ['add_more_here_if_you_need'], 'name': ['']}})
        
        Contains sensor data including temperature and timestamp.
                     Refer to README.md section"Runtime measurement data" for a detailed
                     description of the data structure.
                     
        metadata (dict): {'group_info': {'UUID': ['1ee80b26-48bd-602d-9d9d-40f01e977671'], 'name': ['group_info']},
        'instrument': {'UUID': ['1ee80b26-aad9-653d-af60-03de2f3b9040'], 'name': ['raspberry_pi']},
        'sensor': {'UUID': ['1ee7d8b5-89ef-698d-b53c-47a54e651099', '1ee7d8cb-01f3-6155-bf64-7d793842269d'],
        'name': ['temperature_cup_1', 'temperature_cup_2'], 'serial': ['3ce104574fd4', '3ce1e3805090']},
        'probe': {'UUID': ['1ee80b27-5e16-6232-becb-381aa0d45873'], 'name': ['probe_description']},
        'choose your own type, if you miss one': {'UUID': ['add_more_here_if_you_need'], 'name': ['']}}
        
        Contains metadata. Refer to README.md section "Runtime metadata"
                         for a detailed description of the structure.
                         
        data_folder (str):"/home/pi/calorimetry_home/Archiv"
        
        Path to the folder where the H5 file should be created.
        
        json_folder (str):"/home/pi/calorimetry_home/datasheets"
        
        Path to the folder containing the datasheets.
        #logging_calorimetry(get_meas_data_calorimetry({'group_info': {'UUID': ['1ee80b26-48bd-602d-9d9d-40f01e977671'], 'name': ['group_info']},
        'instrument': {'UUID': ['1ee80b26-aad9-653d-af60-03de2f3b9040'], 'name': ['raspberry_pi']},
        'sensor': {'UUID': ['1ee7d8b5-89ef-698d-b53c-47a54e651099', '1ee7d8cb-01f3-6155-bf64-7d793842269d'],
        'name': ['temperature_cup_1', 'temperature_cup_2'], 'serial': ['3ce104574fd4', '3ce1e3805090']},
        'probe': {'UUID': ['1ee80b27-5e16-6232-becb-381aa0d45873'], 'name': ['probe_description']},
        'choose your own type, if you miss one': {'UUID': ['add_more_here_if_you_need'], 'name': ['']}}),{'group_info': {'UUID': ['1ee80b26-48bd-602d-9d9d-40f01e977671'], 'name': ['group_info']},
        'instrument': {'UUID': ['1ee80b26-aad9-653d-af60-03de2f3b9040'], 'name': ['raspberry_pi']},
        'sensor': {'UUID': ['1ee7d8b5-89ef-698d-b53c-47a54e651099', '1ee7d8cb-01f3-6155-bf64-7d793842269d'],
        'name': ['temperature_cup_1', 'temperature_cup_2'], 'serial': ['3ce104574fd4', '3ce1e3805090']},
        'probe': {'UUID': ['1ee80b27-5e16-6232-becb-381aa0d45873'], 'name': ['probe_description']},
        'choose your own type, if you miss one': {'UUID': ['add_more_here_if_you_need'], 'name': ['']}},"/home/pi/calorimetry_home/Archiv","/home/pi/calorimetry_home/datasheets")


    """
    # Extract the folder name from the provided path to be used as the H5 file name.
    log_name = data_folder.split("/")[-1]
    # Generate the full path for the H5 file.
    dataset_path = "{}/{}.h5".format(data_folder, log_name)
    # Check and create the logging folder if it doesn't exist.
    if not os.path.exists(data_folder):
        os.makedirs(data_folder)

    # Create a new H5 file.
    f = h5py.File(dataset_path, "w")
    # Create a 'RawData' group inside the H5 file.
    grp_raw = f.create_group("RawData")

    # TODO: Add attribute to HDF5.
    # Set attributes for the H5 file based on the datashee
    # DONE #
    f.attrs["created"] = time.strftime("%Y%m%d", time.localtime())
    f.attrs["experiment"] = m_json.get_json_entry(json_folder, metadata['group_info']['UUID'][0],['group','experiment'])
    f.attrs["group_number"] = m_json.get_json_entry(json_folder,metadata['group_info']['UUID'][0],['group','number'])
    f.attrs["authors"] = m_json.get_json_entry(json_folder,metadata['group_info']['UUID'][0],['group','author'])

    # TODO: Write data to HDF5.
        
    UUIDs = ["1ee7d8b5-89ef-698d-b53c-47a54e651099","1ee7d8cb-01f3-6155-bf64-7d793842269d"]
        
    for i in UUIDs:
        sub_group = grp_raw.create_group(i)
        a = sub_group.create_dataset("temperature", data=data[i][0])#temperature
        b = sub_group.create_dataset("timestamp", data=data[i][1])#timestamp
    # DONE #
        print(a[:])
        print(b[:])
    # Close the H5 file.
    
    f.close()

if __name__ == "__main__":
    # Test and debug.
    pass
